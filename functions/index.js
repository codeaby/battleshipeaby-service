const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

const express = require("express");
const cors = require("cors");
const validator = require("express-joi-validation").createValidator({});
const cookieParser = require("cookie-parser")();

// middlewares
const firebaseLoginMiddleware = require("./middlewares/firebase-login.middleware");

// controllers
const roomsController = require("./controllers/rooms.controller");
const boardsController = require("./controllers/boards.controller");

const api = express();

api.use(cors({ origin: true }));
api.use(express.json());
api.use(cookieParser);
api.use(firebaseLoginMiddleware);

// endpoints
api.get("/rooms/available", roomsController.getAvailableRooms);
api.get("/rooms/participating", roomsController.getParticipatingRooms);

api.post(
  "/rooms",
  validator.body(roomsController.postRoomBodySchema),
  roomsController.postRoom
);

api.post("/rooms/:id/join", roomsController.postJoinRoom);

api.get("/rooms/:id/board", boardsController.getMyRoomBoard);

api.post(
  "/rooms/:id/boards",
  validator.body(boardsController.postBoardBodySchema),
  boardsController.postBoard
);

api.post(
  "/rooms/:id/guesses",
  validator.body(boardsController.postGuessBodySchema),
  boardsController.postGuess
);

exports.battleshipeaby = functions.https.onRequest(api);
