const ROOM_STATUS = {
  WAITING_FOR_PLAYER2: "WAITING_FOR_PLAYER2",
  WAITING_PLACEMENT: "WAITING_PLACEMENT",
  PLAYER1_TURN: "PLAYER1_TURN",
  PLAYER2_TURN: "PLAYER2_TURN",
  GAME_OVER: "GAME_OVER",
};

const BOARD = {
  ROWS: 10,
  COLS: 10,
  MAX_SHIPS: 10,
};

module.exports = {
  ROOM_STATUS,
  BOARD,
};
