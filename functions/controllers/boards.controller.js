const joi = require("joi");
const collectionsService = require("../services/collections.service");
const { ROOM_STATUS, BOARD } = require("../constants/constants");

const boardsService = collectionsService("battleshipeaby_boards");
const roomsService = collectionsService("battleshipeaby_rooms");

const getPlayerFromRoom = (room, uid) => {
  if (room.player1.uid === uid) return "player1";
  if (room.player2.uid === uid) return "player2";
  return null;
};

const isPlayerTurn = (room, playerNumber) =>
  (room.status === ROOM_STATUS.PLAYER1_TURN && playerNumber === "player1") ||
  (room.status === ROOM_STATUS.PLAYER2_TURN && playerNumber === "player2");

const enemyPlayer = (playerNumber) =>
  playerNumber === "player1" ? "player2" : "player1";

const nextTurn = (room) =>
  room.status === ROOM_STATUS.PLAYER1_TURN
    ? ROOM_STATUS.PLAYER2_TURN
    : ROOM_STATUS.PLAYER1_TURN;

const initGuesses = () =>
  [...Array(BOARD.ROWS).keys()].map((row) => ({
    row: [...Array(BOARD.COLS).fill(0)],
  }));

const checkWinner = (playerBoard, enemyBoard) =>
  enemyBoard.placements.reduce(
    (winRow, row, rowIndex) =>
      winRow &&
      row.row.reduce(
        (winCell, cell, cellIndex) =>
          winCell &&
          (cell === 0 || playerBoard.guesses[rowIndex].row[cellIndex] === 1),
        true
      ),
    true
  );

module.exports = {
  postBoard: async (req, res) => {
    const {
      params: { id },
      body,
      user: { uid },
    } = req;

    if (!(await roomsService.exists(id))) return res.status(404).send();

    const room = await roomsService.get(id);
    const playerNumber = getPlayerFromRoom(room, uid);

    if (!playerNumber) return res.status(403).send();

    if (room.status !== ROOM_STATUS.WAITING_PLACEMENT)
      return res
        .status(400)
        .send({ message: "Room is not waiting for placement" });

    if (room[playerNumber].board)
      return res
        .status(400)
        .send({ message: `${playerNumber} already placed the board` });

    const newBoard = {
      player: uid,
      placements: body.placements.map((row) => ({ row })),
      guesses: initGuesses(),
    };

    try {
      const boardId = (await boardsService.create(newBoard)).id;

      room[playerNumber].board = boardId;

      if (room.player1.board && room.player2.board)
        room.status = ROOM_STATUS.PLAYER1_TURN;

      await roomsService.update(room.id, room);

      return res.status(204).json(room);
    } catch (e) {
      console.error(e);
      return res.status(500).send();
    }
  },

  postBoardBodySchema: joi.object({
    placements: joi
      .array()
      .items(
        joi
          .array()
          .items(joi.number().min(0).max(BOARD.MAX_SHIPS))
          .length(BOARD.COLS)
          .required()
      )
      .length(BOARD.ROWS)
      .required(),
  }),

  postGuess: async (req, res) => {
    const {
      params: { id },
      body: { guess },
      user: { uid },
    } = req;

    if (!(await roomsService.exists(id))) return res.status(404).send();

    const room = await roomsService.get(id);
    const playerNumber = getPlayerFromRoom(room, uid);

    if (!playerNumber) return res.status(403).send();

    if (!isPlayerTurn(room, playerNumber))
      return res.status(400).send({ message: "It's not your turn" });

    const playerBoard = await boardsService.get(room[playerNumber].board);
    const enemyBoard = await boardsService.get(
      room[enemyPlayer(playerNumber)].board
    );

    const hit = enemyBoard.placements[guess.row].row[guess.col] > 0;
    playerBoard.guesses[guess.row].row[guess.col] = hit ? 1 : -1;

    await boardsService.update(room[playerNumber].board, playerBoard);

    const winner = checkWinner(playerBoard, enemyBoard);

    if (winner) {
      room.status = ROOM_STATUS.GAME_OVER;
      room.winner = uid;
    } else {
      room.status = nextTurn(room);
    }

    await roomsService.update(id, room);

    return res.status(200).json({ hit: hit ? 1 : -1 });
  },

  postGuessBodySchema: joi.object({
    guess: joi
      .object({
        row: joi.number().min(0).max(BOARD.ROWS),
        col: joi.number().min(0).max(BOARD.COLS),
      })
      .required(),
  }),

  getMyRoomBoard: async (req, res) => {
    const {
      params: { id },
      body: { guess },
      user: { uid },
    } = req;

    if (!(await roomsService.exists(id))) return res.status(404).send();

    const room = await roomsService.get(id);
    const playerNumber = getPlayerFromRoom(room, uid);

    if (!playerNumber) return res.status(403).send();

    const board = await boardsService.get(room[playerNumber].board);

    return res.status(200).json({ board });
  },
};
