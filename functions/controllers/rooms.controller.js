const joi = require("joi");
const { ROOM_STATUS } = require("../constants/constants");
const collectionsService = require("../services/collections.service");

const roomsService = collectionsService("battleshipeaby_rooms");

module.exports = {
  getAvailableRooms: async (req, res) => {
    const { uid } = req.user;
    const rooms = await roomsService.getAll((ref) =>
      ref
        .where("status", "==", ROOM_STATUS.WAITING_FOR_PLAYER2)
        .where("player1.uid", "!=", uid)
    );

    return res.json({ rooms });
  },
  getParticipatingRooms: async (req, res) => {
    const { uid } = req.user;
    const createdRooms = await roomsService.getAll((ref) =>
      ref
        .where("status", "!=", ROOM_STATUS.GAME_OVER)
        .where("player1.uid", "==", uid)
    );
    const joinedRooms = await roomsService.getAll((ref) =>
      ref
        .where("status", "!=", ROOM_STATUS.GAME_OVER)
        .where("player2.uid", "==", uid)
    );

    return res.json({ rooms: [...createdRooms, ...joinedRooms] });
  },
  postRoom: async (req, res) => {
    const { uid, name } = req.user;
    const newRoom = Object.assign({}, req.body, {
      player1: {
        uid,
        name,
      },
      status: ROOM_STATUS.WAITING_FOR_PLAYER2,
    });
    const id = (await roomsService.create(newRoom)).id;
    const room = await roomsService.get(id);

    return res.status(201).json(room);
  },
  postRoomBodySchema: joi.object({
    name: joi.string().required(),
  }),
  postJoinRoom: async (req, res) => {
    const {
      params: { id },
      body,
    } = req;
    const { uid, name } = req.user;

    if (!(await roomsService.exists(id))) return res.status(404).send();

    const room = await roomsService.get(id);
    if (room.status !== ROOM_STATUS.WAITING_FOR_PLAYER2) {
      return res
        .status(400)
        .send({ message: "Can't join room. Is full" })
        .send();
    }

    try {
      await roomsService.update(id, {
        player2: {
          uid,
          name,
        },
        status: ROOM_STATUS.WAITING_PLACEMENT,
      });
      return res.status(204).send();
    } catch (e) {
      return res.status(400).send({ message: "Error saving" });
    }
  },
};
